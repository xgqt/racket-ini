#lang info


(define pkg-desc "Racket parser for Ini and UNIX Conf files. Tests.")

(define version "1.0")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"))

(define build-deps
  '("rackunit-lib"
    "ini-lib"))
