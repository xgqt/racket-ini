;; This file is part of racket-ini - Racket parser for Ini and UNIX Conf files.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ini is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ini is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ini.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require scribble/example
          ini
          (for-label racket
                     ini))


@title[#:tag "ini-structs"]{Ini Structures}

@defmodule[ini/structs]

@defstruct[
 ini
 ([sections (listof section?)])
 ]{
}

@defstruct[
 section
 ([name symbol?]
  [params (listof param?)])
 ]{
}

@defstruct[
 param
 ([name symbol?]
  [value string?])
 ]{
}
