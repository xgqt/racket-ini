# Racket-Ini


## About

Racket parser for Ini and UNIX Conf files.


## License

Copyright (c) 2022, Maciej Barć xgqt@riseup.net

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
